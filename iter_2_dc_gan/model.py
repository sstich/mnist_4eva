import numpy as np
import pandas as pd
import time
import torch
import torchvision as tv
import torch.nn as nn
from torch.autograd import Variable
from IPython.display import clear_output
from matplotlib import pyplot as plt

class D(nn.Module):

  def __init__(self):
    super().__init__()
    self.conv_layers = nn.Sequential(
      nn.Conv2d(1, 4, 3),                 # 01x28x28 -> 04x26x26
      nn.BatchNorm2d(4),
      nn.LeakyReLU(0.2, inplace=True),
      nn.Dropout(0.3),
      nn.Conv2d(4, 8, 3),                 # 04x28x28 -> 08x26x26
      nn.BatchNorm2d(8),
      nn.LeakyReLU(0.2, inplace=True),
      nn.Dropout(0.3),
      nn.Conv2d(8, 16, 3),                # 08x26x26 -> 16x24x24
      nn.BatchNorm2d(16),
      nn.LeakyReLU(0.2, inplace=True),
      nn.Dropout(0.3),
      nn.Conv2d(16, 32, 3),               # 16x24x24 -> 32x22x22
      nn.BatchNorm2d(32),
      nn.LeakyReLU(0.2, inplace=True),
      nn.Dropout(0.3),
      nn.Conv2d(32, 1, 3)                 # 32x22x22 -> 01x20x20
    )
    self.linear_layers = nn.Sequential(
      nn.Linear(18*18, 10),
      nn.Linear(10,1),
      nn.Sigmoid()
    )

  def forward(self, x):
    bs = x.shape[0]
    y1 = self.conv_layers(x)
    y2 = self.linear_layers(y1.view(bs, -1))
    return y2

class G(nn.Module):

  def __init__(self):
    super().__init__()
    self.conv_layers = nn.Sequential(
      nn.ConvTranspose2d(1, 64, 3),       # 01x20x20 -> 32x22x22
      nn.BatchNorm2d(64),
      nn.ReLU(True),
      nn.ConvTranspose2d(64, 32, 3),      # 32x22x22 -> 16x24x24
      nn.BatchNorm2d(32),
      nn.ReLU(True),
      nn.ConvTranspose2d(32, 16, 3),       # 16x24x24 -> 08x26x26
      nn.BatchNorm2d(16),
      nn.ReLU(True),
      nn.ConvTranspose2d(16, 8, 3),        # 08x26x26 -> 01x28x28
      nn.BatchNorm2d(8),
      nn.ReLU(True),
      nn.ConvTranspose2d(8, 1, 3),        # 08x26x26 -> 01x28x28
      nn.Tanh()
    )

  def forward(self, z):
    return self.conv_layers(z)

class GANager(object):
    def __init__(
        self,
        G,      D,
        G_loss, D_loss,
        G_opt,  D_opt,
        data_loader,
        device
    ):

        self.G,      self.D      = G,      D
        self.G_loss, self.D_loss = G_loss, D_loss
        self.G_opt,  self.D_opt  = G_opt,  D_opt
        self.data_loader = data_loader
        self.device = device

        self.G.to(self.device)
        self.D.to(self.device)

    def train_G(self):

        # Don't accumulate gradients for D while training G.
        for p in self.D.parameters():
            p.requires_grad = False

        self.G.zero_grad()

        # Train G so that D labels G's fake images as real.
        z = Variable(
            torch.randn(self.data_loader.batch_size, 1, 18, 18)
        ).to(self.device)
        fake_images = self.G(z)
        D_on_fake = self.D(fake_images).view(-1) # This is why we need D's grad off.
        ones = Variable(torch.ones(self.data_loader.batch_size)).to(self.device)
        loss = self.G_loss(
             D_on_fake,
             ones # Want D to be fooled by G.
        )
        loss.backward()
        self.G_opt.step()

        # Turn D's gradient accumulation back on.
        for p in self.D.parameters():
            p.requires_grad = True

        return { 'G_loss': loss.detach().cpu().numpy() }

    def train_D(self, image_batch):

        self.D.zero_grad()

        real_images = Variable(image_batch).to(self.device)
        ones = Variable(torch.ones(self.data_loader.batch_size)).to(self.device)

        # Train D to map real images to 1.
        D_on_real = self.D(real_images).view(-1)
        D_loss_on_real = self.D_loss(
            D_on_real,
            ones
        )
        D_loss_on_real.backward()

        # Train D to map fake (generated) images to 0.
        z = Variable(
            torch.randn(self.data_loader.batch_size, 1, 18, 18)
        ).to(self.device)
        with torch.no_grad(): # Don't accumulate gradients for G while training D
            fake_images_no_grad = self.G(z)
        fake_images = Variable(fake_images_no_grad).to(self.device)
        zeros = Variable(torch.zeros(self.data_loader.batch_size)).to(self.device)
        D_on_fake = self.D(fake_images).view(-1)
        D_loss_on_fake = self.D_loss(
            D_on_fake,
            zeros
        )
        D_loss_on_fake.backward()

        D_loss = D_loss_on_real + D_loss_on_fake
        self.D_opt.step()

        return {
            'D_loss_on_real': D_loss_on_real.detach().cpu().numpy(),
            'D_loss_on_fake': D_loss_on_fake.detach().cpu().numpy(),
            'D_loss'        : D_loss.detach().cpu().numpy(),
            'D_on_real_sum' : D_on_real.detach().cpu().numpy().sum(),
            'D_on_fake_sum' : D_on_fake.detach().cpu().numpy().sum(),
        }

    def train_one_epoch(self):
        start_time = time.time()

        epoch_results = {}
        epoch_results['epoch_D_loss'] = 0
        epoch_results['epoch_G_loss'] = 0
        epoch_results['epoch_D_on_real_sum'] = 0
        epoch_results['epoch_D_on_fake_sum'] = 0

        for i, (images, _) in enumerate(self.data_loader):

            D_epoch_results = self.train_D(images)
            G_epoch_results = self.train_G()

            epoch_results['epoch_D_loss']        += D_epoch_results['D_loss']
            epoch_results['epoch_G_loss']        += G_epoch_results['G_loss']
            epoch_results['epoch_D_on_real_sum'] += D_epoch_results['D_on_real_sum']
            epoch_results['epoch_D_on_fake_sum'] += D_epoch_results['D_on_fake_sum']

        epoch_results['epoch_D_on_real_mean'] = \
                epoch_results['epoch_D_on_real_sum']/(len(self.data_loader)*self.data_loader.batch_size)
        epoch_results['epoch_D_on_fake_mean'] = \
                epoch_results['epoch_D_on_fake_sum']/(len(self.data_loader)*self.data_loader.batch_size)

        end_time = time.time()
        epoch_results['time'] = end_time - start_time
        return epoch_results

    def train(self, n_epochs=50):

        constant_z = Variable(torch.randn(16, 1, 18, 18)).to(self.device)

        epoch_logger = []
        for i in range(n_epochs):
            epoch_results = self.train_one_epoch()
            epoch_results['epoch_index'] = i
            with torch.no_grad():
                epoch_samples = self.G(constant_z)
            image = epoch_samples.detach().cpu().view(16, 28, 28).numpy()
            print(image[0])
            print(image[0].shape)
            block_image = np.block([
                [image[0],  image[1],  image[2],  image[3]],
                [image[4],  image[5],  image[6],  image[7]],
                [image[8],  image[9],  image[10], image[11]],
                [image[12], image[13], image[14], image[15]],
            ])
            epoch_results['samples'] = block_image

            epoch_logger.append(epoch_results)

            clear_output(wait=True)
            for k,v in epoch_results.items():
                if k == 'samples':
                    continue
                print(f"{k}: {v}")
            plt.imshow(block_image)
            plt.show()
            print("\n")

        return epoch_logger

